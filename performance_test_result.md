     K6 Load Test: 2023-12-30 08:41 body { margin: 1rem; } footer { float: right; font-size: 0.8rem; color: #777; } footer a { text-decoration: none; color: #777; } .failed { background-color: #ff6666 !important; } .good { background-color: #3abe3a !important; } td.failed { font-weight: bold; } h2 { padding-bottom: 4px; border-bottom: solid 3px #cccccc; } .tabs { display: flex; flex-wrap: wrap; } .tabs label { order: 1; display: block; padding: 1rem 2rem; margin-right: 0.2rem; cursor: pointer; color: #666; background: #ddd; font-weight: bold; font-size: 1.2rem; flex: 1 1; transition: background ease 0.2s; border-top-left-radius: 0.3rem; border-top-right-radius: 0.3rem; border-color: #ccc; border-style: solid; border-width: 2px 2px 0px; box-shadow: inset 0px -3px 7px -1px rgba(0,0,0,0.33); } .tabs .tab { order: 99; flex-grow: 1; width: 100%; display: none; padding: 1rem; background: #fff; } .tabs input\[type="radio"\] { display: none; } .tabs input\[type="radio"\]:checked + label { background: #fff; box-shadow: none; color: #000; } .tabs input\[type="radio"\]:checked + label + .tab { display: block; } .box { flex: 1 1; border-radius: 0.3rem; background-color: #3abe3a; margin: 1rem; padding: 0.5rem; font-size: 2vw; box-shadow: 0px 4px 7px -1px rgba(0,0,0,0.49); color: white; position: relative; overflow: hidden; } .box h4 { margin: 0; padding-bottom: 0.5rem; text-align: center; position: relative; z-index: 50; } .row { display: flex; } .row div { flex: 1 1; text-align: center; margin-bottom: 0.5rem; } .bignum { position: relative; font-size: min(6vw, 80px); z-index: 20; } table { font-size: min(2vw, 22px); width: 100%; } .icon { position: absolute; top: 60%; left: 50%; transform: translate(-50%, -50%); color: #0000002d; font-size: 8vw; z-index: 1; } .metricbox { background-color: #5697e2; font-size: 3vw; height: auto; } .metricbox .row { position: relative; z-index: 20; }

  K6 Load Test: 2023-12-30 08:41
================================

#### Total Requests

7000

#### Failed Requests

0

#### Breached Thresholds

0

#### Failed Checks

0

  

   Request Metrics

Count

Rate

Average

Maximum

Median

Minimum

90th Percentile

95th Percentile

**http\_req\_duration**

\-

\-

449.99

1500.11

431.50

384.70

483.81

597.23

**http\_req\_waiting**

\-

\-

448.30

1491.53

430.32

378.96

481.48

588.77

**http\_req\_connecting**

\-

\-

18.73

1217.82

\-

\-

111.53

128.41

**http\_req\_tls\_handshaking**

\-

\-

130.31

2288.30

\-

\-

416.43

1281.53

**http\_req\_sending**

\-

\-

0.21

6.03

\-

\-

0.55

0.75

**http\_req\_receiving**

\-

\-

1.48

607.14

0.24

\-

1.81

3.97

**http\_req\_blocked**

\-

\-

161.34

2498.17

\-

\-

692.22

1501.93

**iteration\_duration**

\-

\-

3231.32

5337.41

2887.32

2788.27

4363.52

4646.11

**group\_duration**

\-

\-

611.67

2903.08

433.29

384.70

1339.35

1936.50

  
  
   Note. All times are in milli-seconds

   Other Stats

#### Checks

Passed

45500

Failed

0

#### Iterations

Total

3500

Rate

270.01/s

#### Virtual Users

Min

19

Max

1000

#### Requests

Total

7000

Rate

540.02/s

#### Data Received

Total

7.03 MB

Rate

0.54 mB/s

#### Data Sent

Total

1.27 MB

Rate

0.10 mB/s

 Checks & Groups

• Group - Reqres.in POST API
----------------------------

Check Name

Passes

Failures

response status code is 201 (CREATED)

3500

0

response body not empty

3500

0

response body has 'name' property

3500

0

response body has 'id' property

3500

0

response body has 'job' property

3500

0

'name' value in response body equal to request 'name'

3500

0

'job' value in response body equal to request 'job'

3500

0

  

• Group - Reqres.in PUT API
---------------------------

Check Name

Passes

Failures

response status code is 200 (OK)

3500

0

response body not empty

3500

0

response body has 'name' property

3500

0

response body has 'job' property

3500

0

'name' value in response body equal to request 'name'

3500

0

'job' value in response body not equal to previous 'job' value

3500

0

  

• Other Checks
--------------

Check Name

Passes

Failures

K6 Reporter v2.3.0 - Ben Coleman 2021, [\[GitHub\]](https://github.com/benc-uk/k6-reporter)
